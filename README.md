![](http://cdn.bulbagarden.net/upload/e/e8/560Scrafty.png)

scrafty
---

ES6 promise wrapper around superagent. Use whatever standard-compilant promise lib you want.

Why
---
There's a plenty of ways to integrate superagent and promises, this is just one of them. It has a better API, by making `.end()` return a promise.

Usage
---
Install it

```bash
npm install --save scrafty

```

Just like superagent, but promesified:

```javascript
var request = require('scrafty')(Promise)

request
  .post('/api/pet')
  .send({ name: 'Manny', species: 'cat' })
  .set('X-API-Key', 'foobar')
  .set('Accept', 'application/json')
  .end()
  .then((res) => {
  	// do what you want with my res.body
  })

```

License
---

MIT
